FROM python:3.7

WORKDIR /app

COPY ./requirements.txt /requirements.txt
RUN pip3 --no-cache-dir install -r /requirements.txt

COPY ./src/ /app/
