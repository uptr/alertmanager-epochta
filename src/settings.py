from envparse import env


RECIPIENTS = env.list('RECIPIENTS', subcast=str) or ['79643221687']
EPOCHTA_USERNAME = env.str('EPOCHTA_USERNAME')
EPOCHTA_PASSWORD = env.str('EPOCHTA_PASSWORD')
SENDER = env.str('SENDER')
