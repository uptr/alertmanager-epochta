from aiohttp import web
import requests
import settings

XML_TEMPLATE = """<SMS>     
<operations>      
<operation>SEND</operation>     
</operations>     
<authentification>        
<username>{username}</username>       
<password>{password}</password>       
</authentification>       
<message>   
<sender>{sender}</sender>       
<text>{text}</text>       
</message>        
<numbers>     
<number>{recipient}</number>                 
</numbers>        
</SMS>"""


def messages(alert_payload):
    """Create the message to deliver."""
    for alert in alert_payload['alerts']:
        yield '{} {}'.format(
            alert_payload['status'].upper(),
            alert['annotations'])


async def serve_alert(request):
    alert = await request.json()
    for text in messages(alert):
        xml_message = XML_TEMPLATE.format(
            username=settings.EPOCHTA_USERNAME,
            password=settings.EPOCHTA_PASSWORD,
            sender=settings.SENDER,
            text=text,
            recipient=settings.RECIPIENTS[0]
        )

        print(xml_message)
        response = requests.post(
            'https://api.myatompark.com/members/sms/xml.php',
            data={'XML': xml_message},
        )
        print(response.text)
    return web.Response(body='OK')


web_app = web.Application()
web_app.router.add_post('/alert', serve_alert)
web.run_app(web_app, host='0.0.0.0', port=8080)
